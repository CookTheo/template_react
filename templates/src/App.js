import React from 'react';
import './assets/css/app.css'

// Pages
import Home from './pages/home.js';
import Login from './pages/login.js';
import ErrorPage from './pages/404.js';
import Sweatshirt from './pages/sweatshirt.js';
import DetailsProduct from './pages/detailsProduct.js';

// Components
import NavBar from './components/Header/Header.js';
import StickyFooter from './components/Footer.js';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

function App() {
    return (
        <div className="page-container">
            <div className="App">
                <Router>
                    <NavBar />
                    <Switch>
                        <Redirect from="/home" to="/" />
                        <Route path="/" exact component={Home} />
                        <Route path="/login" component={Login} />
                        <Route path="/sweatshirt" component={Sweatshirt} />
                            <Route path="/sweatshirt+details" component={DetailsProduct} />
                        <Route path="*" component={ErrorPage} />
                    </Switch>
                </Router>
            </div>
            <StickyFooter />
        </div>
    )
}


export default App;