import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

// @material-ui components
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Hidden from '@material-ui/core/Hidden';

// local components
import FilterGender from '../components/Filters/Gender.js';
import FilterPrice from '../components/Filters/Price.js';
import FilterSize from '../components/Filters/Size.js';
import FilterColor from '../components/Filters/Color.js';


// local pictures
import Product1 from '../assets/img/sweatshirt_cut.jpg'

// @material-ui icon
import TuneIcon from '@material-ui/icons/Tune';

import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    maxWidth: {
        width: 'auto',
        maxWidth: '1300px',
        margin: '10px 10px 75px',
        
    },
    settingsGrid:{
        borderBottom:'2px solid #f4f1f6',
        borderTop:'2px solid #f4f1f6',
    },
    sort:{
        alignItems:'right',
        justify:'right',
    },
    nested: {
        paddingLeft: theme.spacing(4),
      },
    flexContainer: {
        justifyContent: 'left',
        display: 'flex',
        flexDirection: 'row',
        height: '40px',
    },
    item: {
        width:'auto',
        position: 'relative',
        padding: '2px',
    },
    noStyle: {
        textDecoration:'none',
    },
}));

function OverviewProducts() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
      setOpen(!open);
    };

    const [filtersOpen, setFiltersOpen] = React.useState(false);
    const filtersDrawerToggle = () => {
        setFiltersOpen(!filtersOpen);
    };
    return (
        <Grid
            container
            spacing={1}
            direction="row"
            justify="flex-start"
            alignItems="center"
            className={classes.maxWidth}
        >

        <h1>Sweat-shirts + Sweats à capuche</h1>&nbsp;<h2 style={{color:'rgba(50,50,50 ,0.4'}}> (5)</h2>
            <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="center"
            className={classes.settingsGrid}
            >

            <Grid item xs={6} md={9} className={classes.filters}>
                <List className={classes.flexContainer}>
                    <Hidden smDown>
                        <Link to="#news" className={classes.noStyle}><ListItem className={classes.item}><Button variant="contained" style={{backgroundColor:'#1b1b1b', color:'#f4f1f6'}} >Nouveautés</Button></ListItem></Link>
                        <ListItem className={classes.item}><FilterGender /></ListItem>
                        <ListItem className={classes.item}><FilterPrice /></ListItem>
                        <ListItem className={classes.item}><FilterSize /></ListItem>
                        <ListItem className={classes.item}><FilterColor /></ListItem>
                    </Hidden>
                    <Hidden mdUp>
                        <ListItem button onClick={filtersDrawerToggle} style={{textAlign:'right'}}>
                            <TuneIcon/><a> &nbsp;Filtres</a>
                        </ListItem>
                    </Hidden>
                </List>
            </Grid>


            <Grid item xs={6} md={3} className={classes.sort}>
            <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            className={classes.root}
            >
                <ListItem button onClick={handleClick}>
                    <ListItemText primary="Trier par" />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit style={{position:'absolute', zIndex:'99',width:'100%'}} className={classes.sort}> 
                    <List component="div" disablePadding style={{backgroundColor:'#f1f1f1',}}>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                            </ListItemIcon>
                            <ListItemText primary="Meilleures ventes" style={{textAlign:'right'}}/>
                        </ListItem>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                            </ListItemIcon>
                            <ListItemText primary="Nouveautés" style={{textAlign:'right'}} />
                        </ListItem>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                            </ListItemIcon>
                            <ListItemText primary="Prix : décroissant" style={{textAlign:'right'}} />
                        </ListItem>
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                            </ListItemIcon>
                            <ListItemText primary="Prix : croissant" style={{textAlign:'right'}} />
                        </ListItem>
                    </List>
                </Collapse>
            </List>
            </Grid>

            </Grid>




            <Grid item xs={6} sm={4} md={3}>
                <Card className={classes.root}>
                        <CardActionArea href="sweatshirt+details">
                            <CardMedia
                            component="img"
                            alt="Sweatshirt"
                            height="300"
                            image={Product1}
                            title="Sweatshirt"
                            />
                            <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                Sweatshirt
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                69.90 CHF
                            </Typography>
                            </CardContent>
                        </CardActionArea>
                </Card>
            </Grid>
            <Grid item xs={6} sm={4} md={3}>
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia
                        component="img"
                        alt="Sweatshirt"
                        height="300"
                        image={Product1}
                        title="Sweatshirt"
                        />
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            Sweatshirt
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            69.90 CHF
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </Grid>
            <Grid item xs={6} sm={4} md={3}>
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia
                        component="img"
                        alt="Sweatshirt"
                        height="300"
                        image={Product1}
                        title="Sweatshirt"
                        />
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            Sweatshirt
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            69.90 CHF
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </Grid>
            <Grid item xs={6} sm={4} md={3}>
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia
                        component="img"
                        alt="Sweatshirt"
                        height="300"
                        image={Product1}
                        title="Sweatshirt"
                        />
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            Sweatshirt
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            69.90 CHF
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </Grid>
            <Grid item xs={6} sm={4} md={3}>
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia
                        component="img"
                        alt="Sweatshirt"
                        height="300"
                        image={Product1}
                        title="Sweatshirt"
                        />
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            Sweatshirt
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            69.90 CHF
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </Grid>
            
        </Grid>
        );
    }
    
    export default OverviewProducts;