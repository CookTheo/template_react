import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import sweatshirt from '../assets/img/sweatshirt.png';




const useStyles = makeStyles((theme) => ({
    cardText: {
        height: '90vh',
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        padding: '50px',
        maxHeight: '800px',
        maxWidth: '900px',
    },
    cardImage: {
        height: '90vh',
        backgroundImage: "url(" + sweatshirt + ")",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        maxHeight: '800px',
        maxWidth: '900px',
    },
    maxWidth: {
        maxWidth: '1800px',
    }
}));

function FeaturedProduct() {
    const classes = useStyles();
    return (
        <Grid
            container
            xs={10}
            direction="row"
            justify="flex-start"
            alignItems="center"
            className={classes.maxWidth}
        >
            <Grid
                container
                xs={12}
                sm={8}
                md={6}
                direction="row"
                justify="flex-start"
                alignItems="center"
                className={classes.cardText}
            >
                <p>
                    <h1>Sweatshirt Black / White</h1>
                    <h2>CHF 55 .-</h2>
                </p>
            </Grid>
            <Grid
                item
                xs={12}
                sm={4}
                md={6}
                direction="row"
                justify="flex-start"
                alignItems="center"
                className={classes.cardImage}
            />
        </Grid>
    );
}

export default FeaturedProduct;