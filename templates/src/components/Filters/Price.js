import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

// @material-ui components
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme) => ({
    width:{
        width:'100%',
        margin:'5px',
        padding:'5px',
    }
}));

function FilterPrice() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const [checked, setChecked] = React.useState([0]);

    const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
    };
    const handleClick = () => {
      setOpen(!open);
    };
    return (
        <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            className={classes.width}
            >
                <ListItem button onClick={handleClick}  variant="outlined">
                    <ListItemText primary="Prix"/>
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit style={{position:'absolute', zIndex:'1',width:'220px', backgroundColor:'#f1f1f1'}}> 
                    <List className={classes.root}>
                        <ListItem key={0} role={undefined} dense button onClick={handleToggle(0)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(0) !== -1}
                                tabIndex={-0}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 0 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={0} primary={`0 - 50`} style={{textAlign:'right'}}/>
                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={1} role={undefined} dense button onClick={handleToggle(1)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(1) !== -1}
                                tabIndex={-1}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 1 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={1} primary={`51 - 100`} style={{textAlign:'right'}}/>
                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={2} role={undefined} dense button onClick={handleToggle(2)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(2) !== -1}
                                tabIndex={-2}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 2 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={2} primary={`101 - 150`} style={{textAlign:'right'}}/>
                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={3} role={undefined} dense button onClick={handleToggle(3)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(3) !== -1}
                                tabIndex={-3}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 3 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={3} primary={`+ 150`} style={{textAlign:'right'}}/>
                        </ListItem>
                    </List>
                </Collapse>
            </List>
    );
}

export default FilterPrice;