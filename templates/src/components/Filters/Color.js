import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

// @material-ui components
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';

// @material-ui icone
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles((theme) => ({
    width:{
        width:'100%',
        margin:'5px',
        padding:'5px',
    }
}));

function FilterColor() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const [checked, setChecked] = React.useState([0]);

    const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
    };
    const handleClick = () => {
      setOpen(!open);
    };
    return (
        <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            className={classes.width}
            >
                <ListItem button onClick={handleClick}  variant="outlined">
                    <ListItemText primary="Couleur"/>
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit style={{position:'absolute', zIndex:'1',width:'220px', backgroundColor:'#f1f1f1'}}> 
                    <List className={classes.root}>
                        <ListItem key={0} role={undefined} dense button onClick={handleToggle(0)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(0) !== -1}
                                tabIndex={-0}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 0 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={0} style={{textAlign:'right'}}>Noir <IconButton style={{color:'Black'}}><FiberManualRecordIcon /></IconButton></ListItemText>
                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={1} role={undefined} dense button onClick={handleToggle(1)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(1) !== -1}
                                tabIndex={-1}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 1 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={1} style={{textAlign:'right'}}>Blanc <IconButton style={{color:'White'}}><FiberManualRecordIcon /></IconButton></ListItemText>
                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={2} role={undefined} dense button onClick={handleToggle(2)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(2) !== -1}
                                tabIndex={-2}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 2 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={2} style={{textAlign:'right'}}>Gris <IconButton style={{color:'Grey'}}><FiberManualRecordIcon /></IconButton></ListItemText>                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={3} role={undefined} dense button onClick={handleToggle(3)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(3) !== -1}
                                tabIndex={-3}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 3 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={3} style={{textAlign:'right'}}>Bleu <IconButton style={{color:'Blue'}}><FiberManualRecordIcon /></IconButton></ListItemText>                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={4} role={undefined} dense button onClick={handleToggle(4)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(4) !== -1}
                                tabIndex={-4}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 4 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={4} style={{textAlign:'right'}}>Rouge <IconButton style={{color:'Red'}}><FiberManualRecordIcon /></IconButton></ListItemText>                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={5} role={undefined} dense button onClick={handleToggle(5)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(5) !== -1}
                                tabIndex={-5}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 5 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={5} style={{textAlign:'right'}}>Vert <IconButton style={{color:'Green'}}><FiberManualRecordIcon /></IconButton></ListItemText>                        </ListItem>
                    </List>
                    <List className={classes.root}>
                        <ListItem key={6} role={undefined} dense button onClick={handleToggle(6)}>
                            <ListItemIcon>
                                <Checkbox
                                edge="start"
                                checked={checked.indexOf(6) !== -1}
                                tabIndex={-6}
                                disableRipple
                                inputProps={{ 'aria-labelledby': 6 }}
                                />
                            </ListItemIcon>
                            <ListItemText id={6} style={{textAlign:'right'}}>Violet <IconButton style={{color:'Purple'}}><FiberManualRecordIcon /></IconButton></ListItemText>                        </ListItem>
                    </List>
                </Collapse>
            </List>
    );
}

export default FilterColor;