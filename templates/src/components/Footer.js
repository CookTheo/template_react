import React from 'react';
import '../assets/css/footer.css'
// component material-ui
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

// component
import Copyright from './Copyright';

// icon
import TwitterIcon from '@material-ui/icons/Twitter';
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';
import InstagramIcon from '@material-ui/icons/Instagram';
import TelegramIcon from '@material-ui/icons/Telegram';
import Swizerland from '@material-ui/icons/LocalHospital';

const useStyles = makeStyles((theme) => ({
    root: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
    },
    main: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(2),
    },
    footer: {
        position: 'relative',
        bottom: 0,
        padding: theme.spacing(3, 2),
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
    },
    copyright: {
        textAlign: 'right',
    },
    icon: {
        padding: '10px'
    }
}));

export default function StickyFooter() {
    const classes = useStyles();

    return (
        <Grid
            container
            component="main"
            className={`${classes.footer} ${'main-footer'}`}
            direction="row"
            justify="flex-start"
            alignItems="center"
        >
            <Grid item xs={12} sm={6} style={{ marginBottom: '50px' }}>
                <Container maxWidth="xs">
                    <Typography variant="h6" style={{ paddingLeft: '10px' }}>Suivez-nous</Typography>
                    <Grid container direction="row" justify="flex-start" alignItems="center">
                        <Grid className={classes.icon}><TwitterIcon /></Grid>
                        <Grid className={classes.icon}><FacebookIcon /></Grid>
                        <Grid className={classes.icon}><YouTubeIcon /></Grid>
                        <Grid className={classes.icon}><InstagramIcon /></Grid>
                        <Grid className={classes.icon}><TelegramIcon /></Grid>
                    </Grid>
                </Container>
            </Grid>
            <Grid item xs={12} sm={6} className={classes.copyright} style={{ marginBottom: '50px' }}>
                <Container maxWidth="xs">
                    <div style={{ display: "inline-flex" }}>
                        <div style={{ paddingTop: "2px", paddingRight: "10px" }}>Suisse</div>
                        <Swizerland style={{ color: 'red' }} />
                    </div>
                    <Copyright />
                </Container>
            </Grid>

        </Grid >
    );
}