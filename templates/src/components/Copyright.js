import React from 'react';

// @material-ui components
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary">
            {'© '}
            {new Date().getFullYear()}
            {' '}
            <Link color="inherit" href="/">
                En Despi
            </Link>{'. '}Tous droits réservés
        </Typography>
    );
}

export default Copyright;