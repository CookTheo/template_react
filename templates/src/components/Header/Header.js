import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import "../../assets/css/header.css"

// material-ui/core - components
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Hidden from "@material-ui/core/Hidden";
import Drawer from "@material-ui/core/Drawer";

// components 
import ResponsiveLinks from './HeaderLinksResponsive';
import SearchToggle from '../SearchToggle';

// icons
import LocalMallIcon from '@material-ui/icons/LocalMall';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Menu from "@material-ui/icons/Menu";
import SearchIcon from '@material-ui/icons/Search';


// local pictures
import Logo from '../../assets/img/enDespi.PNG'


import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        align: 'center',
    },
    toolbarTitle: {
        flex: 1,
    },
    nav: {
        color: 'white',
        textDecoration: 'underline #2b2b2b',
    },
    white: {
        color: '#fafafa',
    },
    backDarkGrey: {
        backgroundColor: '#1b1b1b',
        height: '62px',
    },
    flexContainer: {
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'row',
        padding: 10,
        margin: 'auto',
        overflow: 'hidden',
        height: '60px'
    },
    item: {
        textAlign: 'center',
        fontSize: '100%',
        margin: 0,
        padding: 'auto',
        border: 0,
        color: '#fafafa',
    },
    link: {
        padding: '10px',
        textDecoration: 'none',
    },
    listResponsive: {
        width: '300px',
    },
}));

function NavBar(props) {
    const classes = useStyles();


    const [mobileOpen, setMobileOpen] = React.useState(false);
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const [searchOpen, setSearchOpen] = React.useState(false);
    const searchDrawerToggle = () => {
        setSearchOpen(!searchOpen);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.backDarkGrey}>
                <Toolbar>
                    <Typography variant="inherit" className={classes.title}>
                        <Link to="/" className={classes.nav}>
                            <img src={Logo} alt="En Despi. logo" height="40vh" />
                        </Link>
                    </Typography>
                    <Hidden smDown>
                        <Typography
                            color="inherit"
                            align="center"
                            noWrap
                            className={classes.toolbarTitle}
                        >
                            <List className={classes.flexContainer}>
                                <Link to="/news" className={`${classes.link} ${'linkHover'}`}><ListItem className={classes.item}>Nouveautés</ListItem></Link>
                                <Link to="/sweatshirt" className={`${classes.link} ${'linkHover'}`}> <ListItem className={classes.item}>Homme</ListItem></Link>
                                <Link to="/woman" className={`${classes.link} ${'linkHover'}`}> <ListItem className={classes.item}>Femme</ListItem></Link>
                                <Link to="/kid" className={`${classes.link} ${'linkHover'}`}> <ListItem className={classes.item}>Enfant</ListItem></Link>
                                <Link to="/collections" className={`${classes.link} ${'linkHover'}`}> <ListItem className={classes.item}>Collections</ListItem></Link>
                            </List>
                        </Typography>

                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={searchDrawerToggle}
                        >
                            <SearchIcon />
                        </IconButton>

                        <Link to="/login">
                            <IconButton edge="start" className={classes.white} aria-label="login">
                                <AccountCircleIcon />
                            </IconButton>
                        </Link>
                        <Divider orientation="vertical" flexItem style={{ backgroundColor: "white", margin: "10px" }} />
                        <Link to="/cart">
                            <IconButton className={classes.menu} style={{ color: "#fafafa" }} aria-label="basket">
                                <LocalMallIcon />
                            </IconButton>
                        </Link>
                    </Hidden>
                    <Hidden mdUp>

                        <div style={{ right: '24px', position: 'absolute' }} >
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={searchDrawerToggle}
                            >
                                <SearchIcon />
                            </IconButton>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={handleDrawerToggle}
                            >
                                <Menu />
                            </IconButton>
                        </div>
                    </Hidden>
                </Toolbar>
                <Drawer
                    variant="temporary"
                    anchor={"top"}
                    open={searchOpen}
                    onClose={searchDrawerToggle}
                >
                    <SearchToggle />
                </Drawer>
                <Hidden mdUp implementation="js">
                    <Drawer
                        variant="temporary"
                        anchor={"right"}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                    >
                        <ResponsiveLinks />
                    </Drawer>
                </Hidden>
            </AppBar>
        </div >
    );
}
export default NavBar;