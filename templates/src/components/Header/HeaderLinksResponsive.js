import React from 'react';
import "../../assets/css/header.css"
import { makeStyles } from '@material-ui/core/styles';

// material-ui components
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from "@material-ui/core/Button";
import Divider from '@material-ui/core/Divider';

// icons
import LocalMallIcon from '@material-ui/icons/LocalMall';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles((theme) => ({
    listResponsive: {
        width: '300px',
    },
}));

export default function ResponsiveLinks() {
    const classes = useStyles();

    return (
        <div>
            <List className={classes.listResponsive}>
                <ListItem>
                    <Button
                        href="/news"
                        color="transparent"
                        size="large"
                        style={{ width: '100%' }}
                    >
                        Nouveautés
                    </Button>
                </ListItem>
                <Divider />
                <ListItem>
                    <Button
                        href="/man"
                        color="transparent"
                        size="large"
                        style={{ width: '100%' }}
                    >
                        Homme
                    </Button>
                </ListItem>
                <ListItem>
                    <Button
                        href="/woman"
                        color="transparent"
                        size="large"
                        style={{ width: '100%' }}
                    >
                        Femme
                    </Button>
                </ListItem>
                <ListItem>
                    <Button
                        href="/kid"
                        color="transparent"
                        size="large"
                        style={{ width: '100%' }}
                    >
                        Enfant
                    </Button>
                </ListItem>
                <Divider />
                <ListItem>
                    <Button
                        href="/collections"
                        color="transparent"
                        size="large"
                        style={{ width: '100%' }}
                    >
                        Collections
                    </Button>
                </ListItem>
                <br /><br />
                <ListItem>
                    <Button href="/cart" color="transparent" size="large" style={{ width: '50%' }}>
                        <LocalMallIcon />
                    </Button>
                    <Button href="/login" color="transparent" size="large" style={{ width: '50%' }}>
                        <AccountCircleIcon />
                    </Button>
                </ListItem>
            </List>
        </div>
    );
}