import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';

import SearchIcon from '@material-ui/icons/Search';
const useStyles = makeStyles((theme) => ({
}));
export default function SearchingToggle() {
    const classes = useStyles();
    return (
        <AppBar className={classes.searchBar} position="relative" color="default" elevation={0}>
            <Toolbar>
                <Grid container spacing={2} alignItems="center">
                    <Grid item>
                        <SearchIcon className={classes.block} color="inherit" />
                    </Grid>
                    <Grid item xs>
                        <TextField
                            focused
                            fullWidth
                            placeholder="Rechercher"
                            InputProps={{
                                disableUnderline: true,
                                className: classes.searchInput,
                            }}
                        />
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}