import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

import FeaturedProduct from '../components/FeaturedProduct.js';

import bg1 from '../assets/img/bgHome.png';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 0),
    },
    cardText: {
        height: '600px',
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        padding: '50px'
    },
    bg1: {
        paddingBottom: "50px",
        backgroundImage: "url(" + bg1 + ")",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }
}));

function Home() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container
                xs={12}
                direction="column"
                justify="flex-end"
                alignItems="center"
                className={classes.bg1}
                style={{ width: '100%' }}
            >
                <FeaturedProduct />
            </Grid>
        </div>
    );
}

export default Home;