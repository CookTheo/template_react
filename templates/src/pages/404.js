import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';

import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
    maxWidth: {
        maxWidth: '1200px',
        margin: 'auto',
        padding: theme.spacing(0, 4),
    },
    padding: {
        padding: theme.spacing(5, 0),
    }
}));

function ErrorPage() {
    const classes = useStyles();
    return (
        <Grid
            container
            xs={10}
            direction="row"
            justify="flex-start"
            alignItems="center"
            className={classes.maxWidth}
        >
            <Grid
                item
                xs={12}
                direction="row"
                justify="flex-start"
                alignItems="center"
            >
                <Typography component="h1" variant='h4' className={classes.padding}>
                    <strong>
                        Oups ! La page est introuvable ...
                    </strong>
                </Typography>
            </Grid>
            <Grid
                item
                xs={12}
                direction="row"
                justify="flex-start"
                alignItems="center"
                style={{ marginBottom: '20px' }}
            >
                <AppBar className={classes.searchBar} position="relative" color="default" elevation={0}>
                    <Toolbar>
                        <Grid container spacing={2} alignItems="center">
                            <Grid item>
                                <SearchIcon className={classes.block} color="inherit" />
                            </Grid>
                            <Grid item xs>
                                <TextField
                                    fullWidth
                                    placeholder="Rechercher"
                                    InputProps={{
                                        disableUnderline: true,
                                        className: classes.searchInput,
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Toolbar>
                </AppBar>
            </Grid>

        </Grid>
    );
}

export default ErrorPage;