import React from 'react';

// @material-ui components
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PersonIcon from '@material-ui/icons/Person';
import Typography from '@material-ui/core/Typography';
import { TextField } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

// background image
import image from "../assets/img/bg2.jpg";
import { Redirect } from 'react-router';

class LoginPage extends React.Component {
  constructor() {
    super();
    this.state = {
      input: {},
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let input = this.state.input;
    input[event.target.name] = event.target.value;

    this.setState({
      input
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.validate()) {
      console.log(this.state);

      let input = {};
      input["email"] = "";
      input["password"] = "";
      this.setState({ input: input });
    }
  }

  validate() {
    let input = this.state.input;
    let errors = {};

    if (!input["email"]) {
      errors["email"] = "Entrer une adresse e-mail.";
    }

    if (typeof input["email"] !== "undefined") {

      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(input["email"])) {
        errors["email"] = "Adresse e-mail non valide. (Votre.nom@domain.ch)";
      } else {
        if (input["email"] === "theocook@bluewin.ch" && input["password"] === "1234") {
          window.location.href = "/home"
        } else {
          errors["email"] = "Adresse e-mail ou mot de passe incorrect.";
        }
      }
    }
    this.setState({
      errors: errors
    });

    return <Redirect to="/" />;
  }

  render() {
    return (
      <Grid container component="main" style={{ height: '100vh' }}>
        <Grid item xs={false} sm={4} md={7} style={{
          backgroundImage: "url(" + image + ")",
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          backgroundPosition: 'center',
        }} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={0} square style={{ backgroundColor: "#fff", }}>
          <div style={{
            margin: "20px",
            marginTop: "20%",
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}>
            <Avatar style={{
              backgroundColor: "#222"
            }}>
              <PersonIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Se connecter
          </Typography>
            <form style={{
              width: '100%', // Fix IE 11 issue.
              marginTop: (1),
            }}
              onSubmit={this.handleSubmit}>
              <div style={{ margin: "20px", marginBottom: "0px" }}>
                {this.state.errors.email && <Alert severity="error" style={{ color: "red" }}>{this.state.errors.email}</Alert>}
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  value={this.state.input.email}
                  onChange={this.handleChange}
                  id="email"
                  type="email"
                  label="Adresse e-mail"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  required
                />
              </div>
              <div style={{ margin: "20px", marginTop: "0px", marginBottom: "0px" }}>
                <TextField
                  variant="outlined"
                  margin="normal"
                  value={this.state.input.password}
                  onChange={this.handleChange}
                  fullWidth
                  name="password"
                  label="Mot de passe"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  required
                />
              </div>
              <div style={{ margin: "20px" }}>
                {<Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  Se connecter
                </Button>}
              </div>
            </form>
          </div>
        </Grid>
      </Grid >
    );
  }
}
export default LoginPage;