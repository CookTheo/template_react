import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

// @material-ui components
import Grid from '@material-ui/core/Grid';
import Hidden from "@material-ui/core/Hidden";
import ListItem from '@material-ui/core/ListItem';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 0),
    },
    maxWidth: {
        width: 'auto',
        maxWidth: '1300px',
        margin: '10px 10px 75px',
        
    },
    form:{
        padding: '0px 0px 20px 20px!important',
    },
    item: {
        width:'auto',
        position: 'relative',
        padding: '2px',
    },
}));

const itemData = [
    {
      img: 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,b_rgb:f5f5f5/2a62a796-c741-4309-9741-449cb5e3f16c/haut-sportswear-club-fleece-jsm5Fd.png',
      title: 'Front',
    },
    {
      img: 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,b_rgb:f5f5f5,q_80/40d246f0-ec38-404f-98c7-59e4633716db/haut-sportswear-club-fleece-jsm5Fd.png',
      title: 'Back',
    },
    {
      img: 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,b_rgb:f5f5f5,q_80/215a5d93-cf1a-44e3-a92a-2852263e56ca/haut-sportswear-club-fleece-jsm5Fd.png',
      title: 'Dressed',
    },
    {
        img: 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,b_rgb:f5f5f5/2a62a796-c741-4309-9741-449cb5e3f16c/haut-sportswear-club-fleece-jsm5Fd.png',
        title: 'Front',
      },
      {
        img: 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,b_rgb:f5f5f5,q_80/40d246f0-ec38-404f-98c7-59e4633716db/haut-sportswear-club-fleece-jsm5Fd.png',
        title: 'Back',
      },
      {
        img: 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,b_rgb:f5f5f5,q_80/215a5d93-cf1a-44e3-a92a-2852263e56ca/haut-sportswear-club-fleece-jsm5Fd.png',
        title: 'Dressed',
      },
  ];

function DetailsProduct() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Hidden smDown><p style={{marginLeft:'30px'}}><a>Accueil </a>|<a> Sweat-shirts + Sweats à capuche </a>|<a> Homme </a>|<a> Sweatshirt</a></p></Hidden>
            <Grid container
                xs={12}
                direction="column"
                justify="flex-end"
                alignItems="center"
            >
                <Grid
                    container
                    spacing={1}
                    direction="row"
                    justify="flex-start"
                    className={classes.maxWidth}
                >
                    <Grid container xs={12} md={8} className={classes.pictures}>
                        {itemData.map((item) => (
                            <Grid item xs={6}>
                                <Card className={classes.root}>
                                    
                                        <CardActionArea key={item.img}>
                                            <CardMedia
                                            component="img"
                                            alt={item.title}
                                            image={`${item.img}`}
                                            title={item.title}
                                            />
                                        </CardActionArea>
                                    
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                    <Grid item xs={12} md={4} className={classes.form}>
                        <Grid container>
                            <Grid container xs={7}>
                                <h1 style={{height:'auto'}}>Sweatshirt enfin je crois</h1>
                            </Grid>
                            <Grid container xs={5} direction="row-reverse">
                                <div>
                                        <h3 style={{textAlign:'right', paddingTop:'10px', marginBottom:'5px'}}>CHF 69.90</h3>
                                        <h5 style={{textAlign:'right', marginTop:'0px'}}>(Prix incl. 7.7% TVA)</h5>
                                    </div>
                                    <div>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                        <h5>Choisir un coloris</h5>
                        </Grid>
                           
                            
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default DetailsProduct;