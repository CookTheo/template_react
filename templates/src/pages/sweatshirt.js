import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

// @material-ui components
import Grid from '@material-ui/core/Grid';
import Hidden from "@material-ui/core/Hidden";

import OverviewProducts from '../components/OverviewProducts.js';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 0),
    },
}));

function Sweatshirt() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Hidden smDown><p style={{marginLeft:'30px'}}><a>Accueil </a>|<a> Sweat-shirts + Sweats à capuche </a>|<a> Homme </a></p></Hidden>
            <Grid container
                xs={12}
                direction="column"
                justify="flex-end"
                alignItems="center"
                style={{ width: '100%' }}
            >
                <OverviewProducts />
            </Grid>
        </div>
    );
}

export default Sweatshirt;